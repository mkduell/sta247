# Matt Duell
# matt.duell@mail.utoronto.ca
# Student Num: 998047590
# STA247 Assignment 3

ON=1
OFF=0
DEBUG_FUNC=OFF


source("a3duellmatParkingLot.R")
source("a3duellmatSnakesLadders.R")


# Question 2 - Integration:
#
#fromX is the left limit to start integration
#toX is the right limit to end the integration
#fromY is the lower bound for the start of integration
#toY is the desired maximum height that a point could be placed at.
# The above three will (along with the x-axis) determine the target area
# for placing the randomly generated dots. 
# 
#func is the function to graph
#maxPoints will be how many points to graph before stopping.
#
#General Assumption, if a value generated is right on the curve, it will not
#be counted as below the curve
numIntegrate = function(fromX,toX,fromY,toY,func,maxPoints)
{
	numBelowCurve=0

	for(i in 1:maxPoints)
	{
		#generate random x st fromX <= x <= toX
		#generate random y st fromY <= y <= toY
		randPoint = getRandomPoint(fromX,toX,fromY,toY)

		y = func(randPoint[1])
		if(randPoint[2] < y)
		{
			numBelowCurve = numBelowCurve +1
		}
	
	}
	#return the area
	totalArea = (toX - fromX)*(toY-fromY)
	return((totalArea*numBelowCurve)/maxPoints)
}
getRandomPoint = function(minX,maxX,minY,maxY)
{
	x = runif(1,minX,maxX)
	y = runif(1,minY,maxY)
	return (c(x,y))
}




##
# Question 3 - Parking Lot Simulation:
#
# boardVec is a list representing the different spots
# in a parking lot where each element is a vector.
#	Parking spots that are on the outside of the lot will be represented by a single element vector.
#	Parking spots that are on the inside of a lane will be represented by a 2 element vector (for
#	both sides of the lane)
#
#
# carList is a list containing instances of MyStrategy class, which represents a car (with it's associated strategy)
#	and its position (defined in a3duellmatParkingLot.R)
#
# timingslist is a list that contains a list for each island in the parking lot. That list contains
#	numbers which indicate the time that a new parking spot will appear in that particular island.
parkingSim = function(boardVec,carList,timingslist)
{
	if(DEBUG_FUNC)
	{
		print("parkingSim started")
	}
	counter=1
	carParked=FALSE
	winningCar=0
	while(carParked==FALSE)
	{
		#
		# Updates the board depending on which iteration the simulation is currently in,
		# in conjunction with the timingslist to indicate when an island in the lot should 
		# free up a new spot. (defined a3duellmatParkingLot.R)
		retList = updateBoard(boardVec,counter,timingslist)
		boardVec=retList[[1]]
		timingslist=retList[[2]]

		#move all of the 'cars'
		for(i in 1:length(carList))
		{
			car = carList[[i]]
			#only update the position of the car if the move will not cause
			# a collision (defined in a3duellmatParkingLot.R)
			if(willCollide(carList,i,boardVec)==FALSE)
			{
				#
				# Updates the cars position
				#update is (defined in a3duellmatParkingLot.R)
				theList = update(boardVec,car)
				boardVec=theList[[1]]
				car=theList[[2]]
				carList[[i]]=car
			}
			if(car@pos == PARKED)
			{
				if(DEBUG_FUNC)
				{
					print(paste("Car ",car@name," is parked"))
					print(paste("Took ",counter," Iterations"))
				}
				carParked=TRUE
				winningCar=car
				break
			}
		}
		counter=counter+1
	}
	return(list(winningCar,counter))
}


#Question1 snakes and ladders simulation.
#	boardvec - a vector representation of the game board
#	players - a list of GamePlayer classes (from a3duellmatSnakesLadders.R)
#
############################################
snakesAndLaddersSim = function(boardVec,players)
{
	if(DEBUG_FUNC)
	{
		print("snakesAndLaddersSim Start")
	}

	aPlayerWon=FALSE
	counter = 1
	winningPlayer = NA
	#player = NA
	boardSize = length(boardVec)
	while(aPlayerWon==FALSE)
	{
		#loop through players
		for(i in 1:length(players))
		{
			player = players[[i]]
			#each one rolls -> goes to spot on board
			diceRoll = sample(1:6,1)[[1]]

			if(DEBUG_FUNC){print(paste("player ",player@playerNum," rolled a ",diceRoll))}

			currentPosition = player@pos
			
			if(currentPosition+diceRoll < boardSize +1)
			{
				#resolve if the user stays at the location rolled to 
				# or if they advance (ladder) or move back (snake)
				nextPos = resolveNextPos(currentPosition+diceRoll,boardVec)
				if(DEBUG_FUNC){print(paste("player ",player@playerNum," final spot for the turn=",nextPos))}
			}
			else
			{
				if(DEBUG_FUNC){print(paste("player ",player@playerNum," rolled too high. Will stay put"))}
			}
			player@pos=nextPos
			players[[i]]=player
			#Check for a player reaching the end
			if(nextPos ==boardSize)
			{
				if(DEBUG_FUNC){print(paste("player ",player@playerNum," won after",counter+1," turns"))}

				aPlayerWon=TRUE
				winningPlayer = player
				break;
			}			
		}
		if(aPlayerWon==FALSE)
		{
			counter = counter+1
		}
	}


	if(DEBUG_FUNC)
	{
		print("snakesAndLaddersSim End")
	}
	return(list(winningPlayer,counter))

}

